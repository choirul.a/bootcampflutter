import 'dart:async';
import 'dart:developer';

import 'package:sanberappflutter/latihan/state-management/Bloc/event_manager.dart';

class BlocCounter {
  int _counter = 0;
  final _counterStateController = StreamController<int>();

  StreamSink<int> get _inCounter => _counterStateController.sink;

  Stream<int> get counter => _counterStateController.stream;

  final _counterEventController = StreamController<EventManager>();

  Sink<EventManager> get counterEventSink => _counterEventController.sink;

  BlocCounter() {
    _counterEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(EventManager event) {
    if (event is IncrementEvent) {
      _counter++;
    } else
      _counter--;

    _inCounter.add(_counter);
  }

  void dispose() {
    _counterEventController.close();
    _counterStateController.close();
  }
}
