import 'package:flutter/material.dart';

import 'bloc_counter.dart';
import 'event_manager.dart';

class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  final _bloc = BlocCounter();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Block State Management'),
      ),
      body: Center(
          child: StreamBuilder(
        stream: _bloc.counter,
        initialData: 0,
        builder: (BuildContext context, AsyncSnapshot<int> snip) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                'https://raw.githubusercontent.com/felangel/bloc/master/docs/assets/bloc_logo_full.png',
                width: 250,
                height: 250,
              ),
              Text('Press Below Button'),
              Text('${snip.data}'),
            ],
          );
        },
      )),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () {
              print('Increment');
              _bloc.counterEventSink.add(IncrementEvent());
            },
            backgroundColor: Colors.green,
            tooltip: 'Increment',
            child: Icon(Icons.add),
          ),
          SizedBox(
            width: 10,
          ),
          FloatingActionButton(
              onPressed: () {
                _bloc.counterEventSink.add(DecrementEvent());
              },
              backgroundColor: Colors.red,
              tooltip: 'Decrement',
              child: Icon(Icons.remove))
        ],
      ),
    );
  }
}
