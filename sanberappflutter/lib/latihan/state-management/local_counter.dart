import 'package:flutter/material.dart';

class LocalCounter extends StatefulWidget {
  @override
  _LocalCounterState createState() => _LocalCounterState();
}

class _LocalCounterState extends State<LocalCounter> {
  int _counter = 0;
  String _lastAction = 'none';

  void _incrementCounter({int increase = 1}) {
    setState(() {
      _counter += increase;
      _lastAction = 'Increment';
    });
  }

  void _decrementCounter({int decrease = 1}) {
    setState(() {
      _counter -= decrease;
      _lastAction = 'decrement';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Counter Local State'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('You have to press button many time'),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            )
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          FloatingActionButton(
            onPressed: _incrementCounter,
            tooltip: 'Increment',
            child: Icon(Icons.add),
          ),
          SizedBox(
            width: 10,
          ),
          FloatingActionButton(
              onPressed: _decrementCounter,
              tooltip: 'Decrement',
              child: Icon(Icons.remove))
        ],
      ),
    );
  }
}
