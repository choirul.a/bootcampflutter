import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sanberappflutter/tugas/tugas14/model/user_model.dart';
import 'package:sanberappflutter/tugas/tugas14/post_data.dart';

class GetDataApi extends StatefulWidget {
  @override
  _GetDataApiState createState() => _GetDataApiState();
}

class _GetDataApiState extends State<GetDataApi> {
  get id => null;

  getUserData() async {
    var response = await http
        .get(Uri.parse('https://achmadhilmy-sanbercode.my.id/api/v1/profile'));

    var jsonData = jsonDecode(response.body);

    List<User> users = [];

    for (var item in jsonData) {
      User user = User(item["name"], item["email"], item["address"]);
      users.add(user);
    }

    print('panjang users ${users.length}');

    return users;
  }

  void didChangeDependecies() {
    super.didChangeDependencies();
    setState(() {
      getUserData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Get Data'),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PostDataApi()),
                    );
                  },
                  child: Icon(
                    Icons.add,
                    size: 26.0,
                  )))
        ],
      ),
      body: Container(
        child: Card(
          child: FutureBuilder(
              future: getUserData(),
              builder: (context, snapsot) {
                if (snapsot.data == null) {
                  return Container(
                    child: Center(
                      child: Text('Loading...'),
                    ),
                  );
                } else {
                  return ListView.builder(
                      itemCount: snapsot.data.length,
                      itemBuilder: (context, i) {
                        return ListTile(
                          title: Text(snapsot.data[i].name),
                          subtitle: Text(snapsot.data[i].address),
                          trailing: Text(snapsot.data[i].email),
                        );
                      });
                }
              }),
        ),
      ),
    );
  }
}
