import 'package:flutter/material.dart';
import 'package:sanberappflutter/tugas/tugas15/AboutPage.dart';
import 'package:sanberappflutter/tugas/tugas15/HomePage.dart';
import 'package:sanberappflutter/tugas/tugas15/SearchPage.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;

  final List<Widget> _pageIndex = [HomePage(), SearchPage(), AboutPage()];

  void _onItemTaped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _pageIndex.elementAt(_selectedIndex),
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: _selectedIndex,
            onTap: _onItemTaped,
            selectedItemColor: Colors.blue,
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.search), label: 'Search'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.account_circle), label: 'about')
            ]));
  }
}
