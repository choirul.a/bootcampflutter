import 'package:flutter/material.dart';
import 'package:sanberappflutter/tugas/tugas15/MainScreen.dart';

class LoginScreen2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            children: <Widget>[
              Container(
                child: Image.asset(
                  'assets/img/SanberLogo.png',
                  fit: BoxFit.contain,
                ),
                height: 230,
                alignment: Alignment.centerRight,
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    children: [
                      Container(
                          margin: const EdgeInsets.only(bottom: 30),
                          child: Text('Portofolio',
                              style: TextStyle(
                                  color: Color(0xFF033462),
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold)),
                          alignment: Alignment.centerLeft),
                      Container(
                        margin: const EdgeInsets.only(bottom: 30),
                        child: TextField(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                hintText: 'Username',
                                prefixIcon: Icon(Icons.email_outlined))),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 8),
                        child: TextField(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                hintText: 'Password',
                                prefixIcon: Icon(Icons.lock_outlined))),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 20),
                        child: Text('Forgot Password ?'),
                        alignment: Alignment.centerRight,
                      ),
                      Container(
                        width: double.infinity,
                        child: ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MainScreen()));
                            },
                            child: Text(
                              'Login',
                              style: TextStyle(fontSize: 25),
                            ),
                            style: ElevatedButton.styleFrom(
                                padding: const EdgeInsets.all(10))),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30, bottom: 30),
                        child: Row(
                          children: [
                            Expanded(child: Divider(color: Colors.grey)),
                            Container(
                                child: Text('Or signin with'),
                                padding: EdgeInsets.symmetric(horizontal: 10)),
                            Expanded(child: Divider(color: Colors.grey)),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(right: 5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Colors.grey[350],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                      width: 28,
                                      height: 28,
                                      child: Image.asset('assets/img/fb.png')),
                                  Container(
                                    child: Text(
                                      'Facebook',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    margin: EdgeInsets.only(left: 5),
                                  )
                                ],
                              ),
                            ),
                          )),
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(left: 5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Colors.grey[350],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                      width: 29,
                                      height: 29,
                                      child:
                                          Image.asset('assets/img/google.png')),
                                  Text(
                                    'Google',
                                    style: TextStyle(fontSize: 18),
                                  )
                                ],
                              ),
                            ),
                          )),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
