import 'package:flutter/material.dart';

class DrawerScreen2 extends StatefulWidget {
  @override
  _DrawerScreen2State createState() => _DrawerScreen2State();
}

class _DrawerScreen2State extends State<DrawerScreen2> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text('Choirul Anwar'),
          currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage('assets/img/avatar.png')),
          accountEmail: Text('choirul.awa@gmail.com'),
        ),
        DrawerListTile(
          iconData: Icons.group,
          title: 'NewGroup',
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.lock,
          title: 'MySecretGroup',
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.notifications,
          title: 'New Channel CHat',
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.contacts,
          title: 'Contacts',
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.bookmark_border,
          title: 'SaveMessage',
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.phone,
          title: 'Call',
          onTilePressed: () {},
        )
      ],
    ));
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
