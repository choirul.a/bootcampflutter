import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text(
              'About',
              style: TextStyle(color: Color(0xFF033462A)),
            ),
          ),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        body: Center(child: Text("About Page")));
  }
}
