import 'package:flutter/material.dart';

class Hompage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            'Home',
            style: TextStyle(color: Color(0xFF033462)),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Icon(Icons.person_rounded, color: Color(0xFF033462)),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Container(
              child: Text('Welcome,',
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold)),
              alignment: Alignment.centerLeft,
            ),
            Container(
              child: Text('Choirul Anwar', style: TextStyle(fontSize: 20)),
              alignment: Alignment.centerLeft,
            ),
            SizedBox(height: 30),
            GridView.count(
              crossAxisCount: 2,
              childAspectRatio: 1.491,
              children: [
                for (var item in places) Image.asset('assets/img/$item.png')
              ],
            )
          ],
        ),
      ),
    );
  }
}

final places = ['Monas', 'Roma', 'Tokyo', 'Berlin'];
