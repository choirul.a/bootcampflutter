void main(List<String> args) async {
  print('Ready.. Sing..');
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line() async {
  var lirik1 = 'Pernakah kau merasa...';
  return await Future.delayed(Duration(seconds: 5), () => (lirik1));
}

Future<String> line2() async {
  var lirik2 = 'Pernakah kau merasa...';
  return await Future.delayed(Duration(seconds: 3), () => (lirik2));
}

Future<String> line3() async {
  var lirik23 = 'Pernakah kau merasa...';
  return await Future.delayed(Duration(seconds: 2), () => (lirik23));
}

Future<String> line4() async {
  var lirik4 = 'hatimu hampa pernahkah kau merasa hati mu kosong ............';
  return await Future.delayed(Duration(seconds: 1), () => (lirik4));
}
