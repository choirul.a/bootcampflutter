void main(List<String> args) {
  print('Life');
  delayPrint(1, 'never flat').then((message) => print(message));
  print('is');
}

Future delayPrint(int time, String message) {
  var duration = Duration(seconds: time);
  return Future.delayed(duration, () => (message));
}
