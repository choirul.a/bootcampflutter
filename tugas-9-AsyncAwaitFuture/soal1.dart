void main(List<String> args) async {
  var human = Human();

  print('Luffy');
  print('Zoro');
  print('killer');
  print(human.name);
  await human.getData();
  print(human.name);
}

class Human {
  String name = 'Nama character one piece';

  Future<void> getData() async {
    await Future.delayed(Duration(seconds: 3));
    name = 'Usopp';
    print('get data[done]');
  }
}
