import 'dart:io';

void main(List<String> args) {
  var deret = 1;

//Soal no 1
  print('LOOPING PERTAMA');
  while (deret < 20) {
    print('${deret + 1} - I love coding');
    deret++;
  }

  print('\nLOOP KEDUA');
  while (deret >= 2) {
    print('$deret - I will become a mobile developer');
    deret--;
  }
  print('==============================\n');

//Soal no 2
  for (var i = 1; i <= 20; i++) {
    if (i % 2 != 0) {
      if (i % 3 == 0) {
        print('$i - I love Coding');
      } else {
        print('$i - santai');
      }
    } else {
      print('$i - berkualitas');
    }
  }
  print('==============================\n');

//Soal no 3
  for (var j = 0; j <= 3; j++) {
    for (var k = 0; k < 7; k++) {
      stdout.write('#');
    }
    print('#');
  }
  print('==============================\n');

//Soal no 4
  for (var n = 0; n <= 6; n++) {
    for (var m = 0; m <= 6; m++) {
      if (m <= n) {
        stdout.write('#');
      }
    }
    stdout.write('\n');
  }
}
