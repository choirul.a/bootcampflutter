import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_final_project/grid_item.dart';
import 'package:flutter_final_project/service.dart';

import 'movie_model.dart';

class TrendingPage extends StatefulWidget {
  @override
  _TrendingPageState createState() => _TrendingPageState();
}

class _TrendingPageState extends State<TrendingPage> {
  StreamController<int> streamController = StreamController<int>();

  progressbar() => Center(child: CircularProgressIndicator());

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;

    if (auth.currentUser != null) {
      print(auth.currentUser.email);
    }

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.fromLTRB(25, 40, 25, 25),
        child: Column(
          children: [
            Container(
              //color: Colors.amber,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text.rich(TextSpan(children: [
                    TextSpan(
                        text: 'Hello, ',
                        style:
                            TextStyle(color: Color(0xFF033462), fontSize: 30)),
                    TextSpan(
                        text: auth.currentUser.email,
                        style:
                            TextStyle(color: Color(0xFF033462), fontSize: 20))
                  ])),
                  Icon(
                    Icons.settings,
                    color: Color(0xFF033462),
                    size: 30,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
                onTap: () {
                  print('presss');
                },
                child: Ink(
                  padding: EdgeInsets.all(10),
                  height: AppBar().preferredSize.height,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 3,
                          blurRadius: 5,
                          //offset: Offset(2, 1)
                        )
                      ]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Search Movie',
                        style: TextStyle(fontSize: 20, color: Colors.grey),
                      ),
                      Icon(Icons.search),
                    ],
                  ),
                )),
            SizedBox(height: 35),
            Container(
                margin: EdgeInsets.only(bottom: 5),
                alignment: Alignment.centerLeft,
                child: Text(
                  'Trending on Week',
                  style: TextStyle(color: Colors.grey[700], fontSize: 20),
                )),
            Divider(),
            Expanded(
              child: FutureBuilder<List<Movie>>(
                  future: Service.getMovireResults(),
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Text('Error ${snapshot.error}');
                    }
                    if (snapshot.hasData) {
                      streamController.sink.add(snapshot.data.length);
                      return GridView.count(
                        padding: EdgeInsets.only(top: 5.0),
                        crossAxisCount: 2,
                        childAspectRatio: 0.6,
                        mainAxisSpacing: 15,
                        crossAxisSpacing: 10,
                        children: snapshot.data.map((movie) {
                          return GridTile(child: GridItem(movie));
                        }).toList(),
                      );
                    }
                    return progressbar();
                  }),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }
}
