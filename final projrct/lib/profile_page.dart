import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_final_project/login_screen.dart';

class ProfilePage extends StatelessWidget {
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(25, 40, 25, 25),
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                'About Me',
                style: TextStyle(
                  fontSize: 40,
                  color: Color(0xFF033462),
                ),
              ),
            ),
            SizedBox(height: 25),
            Container(
              alignment: Alignment.center,
              child: CircleAvatar(
                radius: 70,
                backgroundImage: AssetImage('assets/img/avatar.png'),
              ),
            ),
            SizedBox(height: 20),
            Container(
              child: Text(
                'Choirul Anwar',
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w400),
              ),
            ),
            SizedBox(height: 40),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 70),
              child: Column(
                children: [
                  Container(
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 15),
                          child: Icon(
                            Icons.mail,
                            size: 30,
                            color: Colors.grey,
                          ),
                        ),
                        Text(
                          'choirul.awa@gmail.com',
                          style: TextStyle(color: Colors.grey, fontSize: 20),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                            margin: EdgeInsets.only(right: 15),
                            child: Container(
                              height: 28,
                              width: 28,
                              child: Image.asset(
                                'assets/img/instagram.png',
                                color: Colors.grey,
                              ),
                            )),
                        Text(
                          'choirul.awa',
                          style: TextStyle(color: Colors.grey, fontSize: 20),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                            margin: EdgeInsets.only(right: 15),
                            child: Container(
                              height: 28,
                              width: 28,
                              child: Image.asset(
                                'assets/img/facebook.png',
                                color: Colors.grey,
                              ),
                            )),
                        Text(
                          'choirul anwar',
                          style: TextStyle(color: Colors.grey, fontSize: 20),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  SizedBox(height: 30),
                  Container(
                    child: Text('Visit'),
                  ),
                  Divider(
                    indent: 60,
                    endIndent: 60,
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Column(
                            children: [
                              Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  height: 28,
                                  width: 28,
                                  child: Image.asset('assets/img/gitlab.png')),
                              Text('choirul.a',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                  ))
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  height: 28,
                                  width: 28,
                                  child: Image.asset('assets/img/guthub.png')),
                              Text('awaaa19',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                  ))
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 30),
                  ElevatedButton(
                      onPressed: () {
                        _signOut().then((value) => Navigator.of(context)
                            .pushReplacement(MaterialPageRoute(
                                builder: (context) => LoginScreen())));
                      },
                      child: Text('Sign Out'))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
