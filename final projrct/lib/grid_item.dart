import 'package:flutter/material.dart';
import 'package:flutter_final_project/movie_model.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class GridItem extends StatelessWidget {
  final Movie movie;
  final imgURL = 'https://image.tmdb.org/t/p/w500';

  const GridItem(this.movie);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 5.0, left: 5, bottom: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 3,
                blurRadius: 4),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Flexible(
              flex: 5,
              child: ClipRRect(
                  clipBehavior: Clip.antiAlias,
                  borderRadius: BorderRadius.circular(20.0),
                  child:
                      Image.network(imgURL + movie.poster, fit: BoxFit.cover))),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text(movie.title,
                style: TextStyle(
                    fontSize: 20,
                    color: Color(0xFF033462),
                    fontWeight: FontWeight.w500)),
          ),
          Container(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
              child: Row(
                children: [
                  Text(
                    movie.vote.toString(),
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  RatingBarIndicator(
                    rating: movie.vote * 5 / 10,
                    itemSize: 20,
                    itemBuilder: (context, index) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
