import 'dart:convert';
import 'package:flutter_final_project/movie_model.dart';
import 'package:http/http.dart' as http;

class Service {
  static Future<List<Movie>> getMovireResults() async {
    var apiKey = '5afd3614a8131eeb2db2aee53bb325cc';
    var apiURL = Uri.https(
        'api.themoviedb.org', '/3/trending/movie/week', {'api_key': '$apiKey'});

    var apiResult = await http.get(apiURL);
    var jsonObject = jsonDecode(apiResult.body);

    List<dynamic> movieResults =
        (jsonObject as Map<String, dynamic>)['results'];

    List<Movie> movies = [];
    for (var i = 0; i < movieResults.length; i++) {
      movies.add(Movie.getMovie(movieResults[i]));
    }
    return movies;
  }
}
