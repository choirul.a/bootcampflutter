import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;

    if (auth.currentUser != null) {
      print(auth.currentUser.email);
    }

    return Scaffold(
      backgroundColor: Colors.white,
      // appBar: AppBar(
      //     backgroundColor: Colors.white,
      //     elevation: 0,
      //     title: Padding(
      //       padding: const EdgeInsets.all(20.0),
      //       child: Text(
      //         'Home',
      //         style: TextStyle(color: Color(0xFF033462)),
      //       ),
      //     )),
      body: Padding(
        padding: EdgeInsets.fromLTRB(25, 40, 25, 25),
        child: Column(
          children: [
            Container(
              //color: Colors.amber,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text.rich(TextSpan(children: [
                    TextSpan(
                        text: 'Hello, ',
                        style:
                            TextStyle(color: Color(0xFF033462), fontSize: 30)),
                    TextSpan(
                        text: auth.currentUser.email,
                        style:
                            TextStyle(color: Color(0xFF033462), fontSize: 20))
                  ])),
                  Icon(
                    Icons.settings,
                    color: Color(0xFF033462),
                    size: 30,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
                onTap: () {
                  print('presss');
                },
                child: Ink(
                  padding: EdgeInsets.all(10),
                  height: AppBar().preferredSize.height,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 3,
                          blurRadius: 5,
                          //offset: Offset(2, 1)
                        )
                      ]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Search Movie',
                        style: TextStyle(fontSize: 20, color: Colors.grey),
                      ),
                      Icon(Icons.search),
                    ],
                  ),
                )),
            SizedBox(height: 35),
            Container(
                margin: EdgeInsets.only(bottom: 5),
                alignment: Alignment.centerLeft,
                child: Text(
                  'Movie Genre',
                  style: TextStyle(color: Colors.grey[700], fontSize: 20),
                )),
            Divider(),
            Expanded(
              child: GridView.count(
                padding: EdgeInsets.only(top: 5),
                crossAxisCount: 2,
                mainAxisSpacing: 15,
                crossAxisSpacing: 10,
                children: [
                  for (var item in genre) //Text('$item'),
                    Container(
                      margin: EdgeInsets.only(
                        right: 5.0,
                        left: 5,
                        bottom: 10,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 3),
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 5, bottom: 5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              'assets/img/$item.png',
                              height: 150,
                              width: 150,
                            ),
                            Text('$item', style: TextStyle(fontSize: 20)),
                          ],
                        ),
                      ),
                    ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

final genre = [
  'Action',
  'Advanture',
  'Biography',
  'Comedy',
  'Family',
  'Fantasy',
  'Horor',
  'Romance',
  'SciFi',
  'Sport'
];
