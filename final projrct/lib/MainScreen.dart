import 'package:flutter/material.dart';
import 'package:flutter_final_project/home_page.dart';
import 'package:flutter_final_project/profile_page.dart';
import 'package:flutter_final_project/trending_page.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;

  final List<Widget> _pageIndex = [HomePage(), TrendingPage(), ProfilePage()];

  void _onItemTaped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageIndex.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          onTap: _onItemTaped,
          selectedItemColor: Colors.blue,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(Icons.trending_up), label: 'Trending'),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle), label: 'about')
          ]),
    );
  }
}
