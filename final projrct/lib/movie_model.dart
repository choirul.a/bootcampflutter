class Movie {
  String title;
  double vote;
  String poster;

  Movie({this.title, this.vote, this.poster});

  factory Movie.getMovie(Map<String, dynamic> object) {
    return Movie(
        title: object['title'],
        vote: object['vote_average'],
        poster: object['poster_path']);
  }
}
