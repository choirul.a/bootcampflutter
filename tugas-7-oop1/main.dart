import 'lingkaran.dart';

void main(List<String> args) {
//Soal no 1
  var segitiga = Segitiga();

  segitiga.setengah = 0.5;
  segitiga.alas = 20.0;
  segitiga.tinggi = 30.0;

  var luasSegitiga = segitiga.luasSegitiga();
  print('Luas Segitiga = $luasSegitiga');

//Soal no 2
  var lingkaran = Lingkaran();

  lingkaran.setJari2(5);

  var luasLingkaran = lingkaran.luasLingkaran;

  print('Luas lingkaran = $luasLingkaran');
}

class Segitiga {
  var setengah, alas, tinggi;

  double luasSegitiga() {
    return setengah * alas * tinggi;
  }
}
