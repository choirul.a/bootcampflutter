class Lingkaran {
  var pi = 3.14;
  var _jari2;

  void setJari2(double value) {
    if (value < 0) {
      value *= -1;
    }
    _jari2 = value;
  }

  double get luasLingkaran => pi * _jari2 * _jari2;
}
