void main(List<String> args) {
  print('==============Soal no 1===============');
  print(teriak());

  print('\n===========Soal no 2==================');
  var num1 = 3;
  var num2 = 4;
  print('hasil dari $num1 x $num2  adalah: ' + kalikan(num1, num2).toString());

  print('\n=============Soal no 3================');
  var name = 'Choirul anwar';
  var umur = 27;
  var address = 'Gresik Jawa Timur';
  var hobby = 'rebahan';

  print(introduce(name, umur, address, hobby));

  print('\n=============Soal no 4================');
  var n = 4;
  print('factorial dari $n adalah ' + factorial(n).toString());
}

//Soal no 1
String teriak() => 'Halo Sanbers!';

//Soal no 2
int kalikan(a, b) => a * b;

//Soal no 3
String introduce(String name, int umur, String address, String hobby) =>
    'Nama saya $name, umur saya $umur tahun, alamat saya di $address, hobby saya adalah $hobby';

//Soal no 4
int factorial(n) {
  if (n == 0 || n == 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}
