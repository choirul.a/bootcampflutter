import 'bangun_datar.dart';

class Lingkaran extends BangunDatar {
  var jari2;

  Lingkaran(double jari2) {
    this.jari2 = jari2;
  }

  @override
  double luas() {
    return 3.14 * jari2 * jari2;
  }

  @override
  double keliling() {
    return 3.14 * 2 * jari2;
  }
}
