import 'dart:math';

import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  var alas;
  var tinggi;

  Segitiga(double alas, double tinggi) {
    this.alas = alas;
    this.tinggi = tinggi;
  }

  @override
  double luas() {
    return 0.5 * alas * tinggi;
  }

  @override
  double keliling() {
    return alas + tinggi + (sqrt((alas * alas) + (tinggi * tinggi)));
  }
}
