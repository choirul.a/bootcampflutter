import 'bangun_datar.dart';

class Persegi extends BangunDatar {
  var sisi;

  Persegi(double sisi) {
    this.sisi = sisi;
  }

  @override
  double luas() {
    return sisi * sisi;
  }

  @override
  double keliling() {
    return 4.0 * sisi;
  }
}
