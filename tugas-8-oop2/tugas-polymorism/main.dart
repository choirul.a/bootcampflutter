import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  var bangunDatar = BangunDatar();
  var persegi = Persegi(5.0);
  var segitiga = Segitiga(3.0, 4.0);
  var lingkaran = Lingkaran(10.0);

  bangunDatar.keliling();
  bangunDatar.luas();

  print('luas persegi = ${persegi.luas()}');
  print('keliling persegi = ${persegi.keliling()}\n');
  print('luas sgeitiga = ${segitiga.luas()}');
  print('keliling segitiga = ${segitiga.keliling()}\n');
  print('luas lingkaran = ${lingkaran.luas()}');
  print('keliling lingkaran = ${lingkaran.keliling()}\n');
}
