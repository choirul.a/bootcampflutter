void main(List<String> args) {
  print('================Soal no 1==================');
  print(range(1, 10));

  print('\n================Soal no 2==================');
  print(rangeWithStep(10, 1, 3));

  print('\n================Soal no 3==================');
  var input = [
    ['0001', 'Roman Alamsyah', 'Bandar Lampung', '21/05/1989', 'Membaca'],
    ['0002', 'Dika Sembiring', 'Medan', '10/10/1992', 'Bermain Gitar'],
    ['0003', 'Winona', 'Ambon', '25/12/1965', 'Memasak'],
    ['0004', 'Bintang Senjaya', 'Martapura', '6/4/1970', 'Berkebun']
  ];

  print(dataHandling(input).join());

  print('================Soal no 4==================');
  print(balikKata('sanbers'));
}

//Soal no 1
List range(firstNumb, lastNumb) {
  var listQuest1 = [];

  if (firstNumb < lastNumb) {
    for (var i = firstNumb; i < lastNumb + 1; i++) {
      listQuest1.add(i);
    }
  } else {
    for (var i = firstNumb; i > lastNumb - 1; i--) {
      listQuest1.add(i);
    }
  }
  return listQuest1;
}

//Soal no 2
List rangeWithStep(firstNumb, lastNumb, step) {
  var listQuest2 = [];

  if (firstNumb < lastNumb) {
    for (var i = firstNumb; i < lastNumb + 1; i += step) {
      listQuest2.add(i);
    }
  } else {
    for (var i = firstNumb; i > lastNumb - 1; i -= step) {
      listQuest2.add(i);
    }
  }
  return listQuest2;
}

//Soal no 3
List dataHandling(List input) {
  var data = [];
  for (var i = 0; i < input.length; i++) {
    data.add(
        'Nomer ID: ${input[i][0]}\nNama Lengkap: ${input[i][1]}\nTTL: ${input[i][2]} ${input[i][3]}\nHobi: ${input[i][4]}\n\n');
  }
  return data;
}

//Soal no 4
String balikKata(String kata) {
  var listKata = [];
  for (var i = kata.length - 1; i >= 0; i--) {
    // print(kata[i]);
    listKata.add(kata[i]);
  }
  return listKata.join();
}
