import 'dart:io';

void main(List<String> args) {
  //Soal 1
  String output;

  print('Apakah anda akan menginstal dart? Y/T');
  var input = stdin.readLineSync();

  output =
      (input == 'Y' || input == 'y') ? "Anda akan menginstal dart" : "Aborted";

  print(output);
  print('======================================\n');

  //Soal 2

  String? nama;
  String? peran;

  print('masukkan nama!');
  nama = stdin.readLineSync();
  print('masukkan peran! (warewolf/penyihir/guard)');
  peran = stdin.readLineSync();

  if (nama == '') {
    print('nama harus di isi');
  } else if (peran == '') {
    print('hai $nama, pilih peranmu untuk memulai game');
  } else {
    if (peran == 'Penyihir' || peran == 'penyihir') {
      print(
          '"Selamat datang di Dunia Werewolf, $nama"\n"Halo $peran $nama,  kamu dapat melihat siapa yang menjadi werewolf!"');
    } else if (peran == 'Guard' || peran == 'guard') {
      print(
          '"Selamat datang di Dunia Werewolf, $nama"\n"Halo $peran $nama,  kamu akan membantu melindungi temanmu dari serangan werewolf"');
    } else if (peran == 'Warewolf' || peran == 'warewolf') {
      print(
          '"Selamat datang di Dunia Werewolf, $nama"\n"Halo $peran $nama,  Kamu akan memakan mangsa setiap malam"');
    } else {
      print('$peran tidak ada dalam daftar peran');
    }
  }
  print('======================================\n');

  //Soal no 3
  print('masukkan hari');
  var hari = stdin.readLineSync();

  switch (hari) {
    case 'senin':
      {
        print(
            'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
        break;
      }
    case 'selasa':
      {
        print(
            'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');

        break;
      }
    case 'rabu':
      {
        print(
            'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
        break;
      }
    case 'kamis':
      {
        print(
            'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
        break;
      }
    case 'jumat':
      {
        print('Hidup tak selamanya tentang pacar.');
        break;
      }
    case 'sabtu':
      {
        print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
        break;
      }
    case 'minggu':
      {
        print(
            'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
        break;
      }
    default:
      print('hari yang dimasukkan salah');
  }
  print('======================================\n');

  //Soal no 4
  var tanggal = 29;
  var bulan = 2;
  var tahun = 2021;

  switch (bulan) {
    case 1:
      {
        print('$tanggal Januari $tahun');
        break;
      }
    case 2:
      {
        if (tahun % 4 != 0) {
          if (tanggal <= 0 || tanggal > 28) {
            print(
                'Bukan tahun kabisat, bulan Februari maksimal sampai tanggal 28');
          } else {
            print('$tanggal Februari $tahun');
          }
        } else {
          if (tanggal <= 0 || tanggal > 29) {
            print('bulan Februari maksimal tanggal 29');
          } else {
            print('$tanggal Februari $tahun');
          }
        }
        break;
      }
    case 3:
      {
        print('$tanggal Maret $tahun');
        break;
      }
    case 4:
      {
        if (tanggal <= 0 || tanggal > 30) {
          print('bulan April maksimal tanggal 30');
        } else {
          print('$tanggal April $tahun');
        }
        break;
      }
    case 5:
      {
        print('$tanggal Mei $tahun');
        break;
      }
    case 6:
      {
        if (tanggal <= 0 || tanggal > 30) {
          print('bulan Juni maksimal tanggal 30');
        } else {
          print('$tanggal Juni $tahun');
        }
        break;
      }
    case 7:
      {
        print('$tanggal Juli $tahun');
        break;
      }
    case 8:
      {
        print('$tanggal Agustus $tahun');
        break;
      }
    case 9:
      {
        if (tanggal <= 0 || tanggal > 30) {
          print('bulan September maksimal tanggal 30');
        } else {
          print('$tanggal September $tahun');
        }
        break;
      }
    case 10:
      {
        print('$tanggal Oktober $tahun');
        break;
      }
    case 11:
      {
        if (tanggal <= 0 || tanggal > 30) {
          print('bulan November maskimal tanggal 30');
        } else {
          print('$tanggal November $tahun');
        }
        break;
      }
    case 12:
      {
        print('$tanggal Desember $tahun');
        break;
      }

    default:
      print('bulan yang dimasukkan salah');
  }
}
