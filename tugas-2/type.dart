import 'dart:io';

main(List<String> args) {
  var first = 'Dart';
  var second = 'is';
  var third = 'awesome';
  var fourth = 'and';
  var fifth = 'I';
  var sixth = 'love';
  var seventh = 'it!';

  print('$first $second $third $fourth $fifth $sixth $seventh');
  print('===============================================');

//Soal nomor 2
  var sentence = "I am going to be Flutter Developer";

  var firstWord = sentence[0];
  var secondWord = sentence[2] + sentence[3];
  var thirdWord =
      sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
  var fourthWord = sentence[11] + sentence[12];
  var fifthWord = sentence[14] + sentence[15];
  var sixthWord = sentence[17] +
      sentence[18] +
      sentence[19] +
      sentence[20] +
      sentence[21] +
      sentence[22] +
      sentence[23];
  var seventhWord = sentence[25] +
      sentence[26] +
      sentence[27] +
      sentence[28] +
      sentence[29] +
      sentence[30] +
      sentence[31] +
      sentence[32] +
      sentence[33];

  print('First Word: ' + firstWord);
  print('Second Word: ' + secondWord);
  print('Third Word: ' + thirdWord);
  print('Fourth Word: ' + fourthWord);
  print('Fifth Word: ' + fifthWord);
  print('Sixth Word: ' + sixthWord);
  print('Seventh Word: ' + seventhWord);
  print('===============================================');

//Soal nomor 3
  print('masukkan nama depan: ');
  var firstName = stdin.readLineSync();

  print('masukkan nama belakang');
  var secondName = stdin.readLineSync();

  print('nama lengkap saya adalah: $firstName $secondName');
  print('===============================================');

//Soal nomer 4

  var numA = 5;
  var numB = 10;

  print('penambahan :' + (numA + numB).toString());
  print('pengurangan :' + (numA - numB).toString());
  print('perkalian :' + (numA * numB).toString());
  print('pembagian :' + (numA / numB).toString());
}
